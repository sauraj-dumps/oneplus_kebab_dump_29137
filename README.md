## aosp_kebab-userdebug 12 SQ1D.220205.004 1645709771 test-keys
- Manufacturer: oneplus
- Platform: kona
- Codename: kebab
- Brand: OnePlus
- Flavor: aosp_kebab-userdebug
- Release Version: 12
- Id: SQ1D.220205.004
- Incremental: 1645709771
- Tags: test-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: true
- Locale: en-US
- Screen Density: undefined
- Fingerprint: OnePlus/kebab/kebab:12/SQ1D.220205.004/1336:userdebug/release-keys
- OTA version: 
- Branch: aosp_kebab-userdebug-12-SQ1D.220205.004-1645709771-test-keys
- Repo: oneplus_kebab_dump_29137


>Dumped by [Phoenix Firmware Dumper](https://github.com/DroidDumps/phoenix_firmware_dumper)
